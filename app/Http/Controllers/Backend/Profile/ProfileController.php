<?php

namespace App\Http\Controllers\Backend\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;

class ProfileController extends Controller
{
    public function index($id)
    {
      $user = User::find($id);

      return view('backend.views.profile.index', compact('user'));
    }

}
