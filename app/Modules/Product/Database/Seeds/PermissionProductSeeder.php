<?php

namespace App\Modules\Product\Database\Seeds;

use Illuminate\Database\Seeder;

class PermissionProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
        [
            'name' => 'ver_productos',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'crear_productos',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'editar_productos',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'eliminar_productos',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'ver_categorias',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'crear_categorias',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'editar_categorias',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'eliminar_categorias',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]
      ]);
      DB::table('role_has_permissions')->insert([
        [
            'permission_id' => '18',
            'role_id' => '1',
        ],
        [
            'permission_id' => '19',
            'role_id' => '1',
        ],
        [
            'permission_id' => '20',
            'role_id' => '1',
        ],
        [
            'permission_id' => '21',
            'role_id' => '1',
        ],
        [
            'permission_id' => '22',
            'role_id' => '1',
        ],
        [
            'permission_id' => '23',
            'role_id' => '1',
        ],
        [
            'permission_id' => '24',
            'role_id' => '1',
        ],
        [
            'permission_id' => '25',
            'role_id' => '1',
        ]
      ]);
    }
}
