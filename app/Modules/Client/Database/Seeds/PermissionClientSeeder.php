<?php

namespace App\Modules\Client\Database\Seeds;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Carbon\Carbon;

class PermissionClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
        [
            'name' => 'ver_clientes',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'crear_clientes',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'editar_clientes',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name' => 'eliminar_clientes',
            'guard_name' => 'web',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]
      ]);
      DB::table('role_has_permissions')->insert([
        [
            'permission_id' => '14',
            'role_id' => '1',
        ],
        [
            'permission_id' => '15',
            'role_id' => '1',
        ],
        [
            'permission_id' => '16',
            'role_id' => '1',
        ],
        [
            'permission_id' => '17',
            'role_id' => '1',
        ]
      ]);
    }
}
