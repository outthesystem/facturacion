@extends('backend.layouts.master')

@section('title')
  <title>{{env('APP_NAME')}} | Perfil {{$user->name}}</title>
@endsection

@section('view')
  <div id="page-container" class="sidebar-o side-scroll sidebar-inverse page-header-modern main-content-boxed">

      @include('backend.partials.aside.default')

      @include('backend.partials.sidebar.default')

      @include('backend.partials.navbar.default')

      <!-- Main Container -->
      <main id="main-container">
        <div class="bg-image bg-image-bottom" style="background-image: url('{{asset('backend/img/photos/photo13@2x.jpg')}}');">
          <div class="bg-primary-dark-op py-30">
              <div class="content content-full text-center">
                  <div class="mb-15">
                      <a class="img-link" href="be_pages_generic_profile.php">
                          <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{asset('backend/img/avatars/avatar15.jpg')}}" alt="">
                      </a>
                  </div>
                  <h1 class="h3 text-white font-w700 mb-10">{{$user->name}}</h1>
                  <h2 class="h5 text-white-op">
                      {{$user->roles->pluck('name')}}
                  </h2>
              </div>
          </div>
      </div>


      <!-- Page Content -->

      <!-- END Page Content -->

      </main>
      <!-- END Main Container -->

      @include('backend.partials.footer.default')
  </div>
@endsection
